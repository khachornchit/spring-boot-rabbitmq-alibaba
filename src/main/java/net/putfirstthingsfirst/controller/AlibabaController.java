package net.putfirstthingsfirst.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import net.putfirstthingsfirst.model.SmsCommand;

@RestController
@RequestMapping("/alibaba")
public class AlibabaController {

	@RequestMapping(value="/subscribe", method=RequestMethod.POST)
	public String subscribe(@RequestBody SmsCommand sms) {
		String message = sms.getPhoneNumber() + " : You are successfully subscribed for " + sms.getRoutekey();
		System.out.println(message);
		return message;
	}
}
